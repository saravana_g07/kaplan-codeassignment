# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Following steps to be followed once clonned the repository ##

# Configure Server side 'KaplanCodeAssignment' server side (API) project #

	- Configure data soucre in web config (The file source already placed and confiured)
	 	 <appSettings>
    		<add key="dataSource" value="Assets/DataSource/channel.json"/>
  		</appSettings>
		
	- Run the application using VisualStudio or IIS
	
# Configure Client side Angular 6 application #

	- Once clone the application install npm using terminal or visual code
		To install change directory and execute npm install command
	
	- Create Channel.json file (in this application the file is created in src/assets/resources location)
	
	- configure baseApiUrl and localJsonFile in global-config.js file (app/shared/resources/global-config.ts)
	
	- run the client side application using npm start command in terminal
	
	**Note : The Session images and Instrudtor images were hard coded in the html due to unavailablity of images in the given json data source file (channel.json)
