import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ChannelRoutingModule } from './channel-routing.module';
import { ChannelInfoComponent } from './channel-info/channel-info.component';

@NgModule({
  imports: [
    CommonModule,
    ChannelRoutingModule,
    FormsModule
  ],
  declarations: [ChannelInfoComponent]
})
export class ChannelModule { }
