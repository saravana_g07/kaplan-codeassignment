export class channelDataSource {
    title: string;
    description: string;
    instructorName: string;
    instructorPhotoUrl: string;
    subjectPhotoUrl: string;
    time: string
}

export class channelInfo {
    title: string;
    description: string;
    instructorName: string;
    instructorPhotoUrl: string;
    subjectPhotoUrl: string;
    date: string;
    time: string
}

export class channelGroupByDate {
    date: string;
    channelInfoList: channelInfo[]
}
