import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChannelInfoComponent } from "./channel-info/channel-info.component";

const routes: Routes = [
  { path: '', component: ChannelInfoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChannelRoutingModule { }
