import { Component, OnInit } from '@angular/core';
import { channelInfo, channelGroupByDate } from "../Model/channel";
import { ChannelService } from "../services/channel.service";
import { dataSourceEnum } from "../../shared/resources/global-config";
import { observable } from 'rxjs';
@Component({
  selector: 'app-channel-info',
  templateUrl: './channel-info.component.html',
  styleUrls: ['./channel-info.component.css'],
  providers: [ChannelService]
})
export class ChannelInfoComponent implements OnInit {

  constructor(private channelService: ChannelService) { }

  //#region Variable declaration

  channelSessionGroupListByDate: channelGroupByDate[];
  selectedDataSource: number;
  dataSourceList: Array<object>;

  //#endregion

  ngOnInit() {
    this.channelSessionGroupListByDate = [];
    this.selectedDataSource = dataSourceEnum.LocalJson;
    this.dataSourceList = this.dataSourceKeys();
    this.LoadPageData();

  }

  dataSourceKeys(): Array<object> {
    let keys = Object.keys(dataSourceEnum);
    let dataSourceEnumValues = keys.slice(keys.length / 2);
    let dataSourceList = [];
    dataSourceEnumValues.forEach(element => {

      dataSourceList.push(new Object({ 'key': element, 'value': dataSourceEnum[element] }));
    });
    return dataSourceList;
  }

  // Get channel source data
  LoadPageData() {
    this.channelSessionGroupListByDate = [];
    this.channelService.getAllChannel(this.selectedDataSource).subscribe(result => {
      let formattedChannelInfoList = this.formatChannelViewData(result);
      this.groupChannelListByDate(formattedChannelInfoList);
      console.log(this.channelSessionGroupListByDate);
    });
  }

  // Formate channel data to bind in the UI
  formatChannelViewData(_channelDataSource: any): Array<channelInfo> {
    let channelInfoList: channelInfo[] = [];
    _channelDataSource.forEach(data => {

      let thisChannelInfo: channelInfo = new channelInfo();

      thisChannelInfo.title = data.title;
      thisChannelInfo.description = data.description;
      thisChannelInfo.subjectPhotoUrl = data.subjectPhotoUrl;
      thisChannelInfo.instructorName = data.instructorName;
      thisChannelInfo.instructorPhotoUrl = data.instructorPhotoUrl;
      thisChannelInfo.date = data.time.toString().split(' ')[0];
      thisChannelInfo.time = this.getSessionTime(data.time.toString().split(' ')[1]);

      channelInfoList.push(thisChannelInfo);

    });
    // this.channelList = channelInfoList;
    return channelInfoList;
  }

  groupChannelListByDate(_channelInfoList: channelInfo[]) {
    let channelGroupValue = [];
    let dateArray = [];

    _channelInfoList.forEach(channel => {
      channelGroupValue[channel.date] = channelGroupValue[channel.date] || [];
      channelGroupValue[channel.date].push(channel);
      if (dateArray.indexOf(channel.date) < 0) {
        dateArray.push(channel.date);
      }
    });

    dateArray.sort();

    dateArray.forEach(date => {
      let channelInfoGroupByDate = new channelGroupByDate();
      channelInfoGroupByDate.date = this.getDayDetail(date);
      channelInfoGroupByDate.channelInfoList = channelGroupValue[date];
      this.channelSessionGroupListByDate.push(channelInfoGroupByDate);
    });

  }

  getSessionTime(time: string) {
    let startHour = time.split(':')[0];
    let minute = time.split(':')[1];
    let endHour = Number.parseInt(startHour) + 1;


    let startTime = this.getDaySessionTime(startHour, minute);
    let endTime = this.getDaySessionTime(endHour.toString(), minute);

    return startTime + ' - ' + endTime + ' EST';
  }

  getDaySessionTime(timeHour: string, minute: string) {
    let daySession = "AM";
    if (timeHour == '00') {
      timeHour = '12';
    }
    else if (Number.parseInt(timeHour) > 12) {
      daySession = "PM";
      timeHour = (Number.parseInt(timeHour) - 12).toString();
    }

    timeHour = timeHour.length == 1 ? '0' + timeHour : timeHour;
    return timeHour + ':' + minute + ' ' + daySession

  }

  getDayDetail(inputDate: string) {
    let year = Number.parseInt(inputDate.split('-')[0]);
    let month = Number.parseInt(inputDate.split('-')[1]);
    let date = Number.parseInt(inputDate.split('-')[2]);
    let dateDetail = new Date(year, month, date);
    let dateDetailInfo = dateDetail.toString().split(' ');

    let monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    let formattedDate = dateDetailInfo[0] + ',';
    formattedDate += monthNames[dateDetail.getMonth() - 1] + ' ';
    formattedDate += dateDetailInfo[2] + ',';
    formattedDate += dateDetailInfo[3];
    return formattedDate;

  }

}
