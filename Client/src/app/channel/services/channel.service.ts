
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { globalConfig, dataSourceEnum } from "../../shared/resources/global-config";

@Injectable()
export class ChannelService {

  //#region local variable declaration
  httpClient: HttpClient;
  baseApiUrl: string;
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, OPTIONS'
  });

  //#endregion local variable declaration

  constructor(private http: HttpClient) {
    this.httpClient = http;
    this.baseApiUrl = globalConfig.baseApiUrl;
  }

  getAllChannel(source: number) {
    if (source == dataSourceEnum.ExternalApi) {
      return this.getFromBaseApi();
    }
    else {
      return this.getFromLocalJson();
    }
  }

  getFromBaseApi() {
    return this.httpClient.get(this.baseApiUrl + 'api/channel/getChannelList')
      // { headers: this.headers })
      .pipe(map(responsedata => {
        let body = JSON.parse(JSON.stringify(responsedata));
        // this data manipulation placed here instead of component, to make binding method as generic 
        // also we are not gonna show any error message here
        if (body.isSuccess) {
          return body.channelList;
        }
        else {
          return [];
        }
      }))

  }
  getFromLocalJson() {
    return this.httpClient.get('assets/resources/channel.json')
      .pipe(map(responsedata => {
        let body = responsedata
        return body || {};
      }))
  }

}
