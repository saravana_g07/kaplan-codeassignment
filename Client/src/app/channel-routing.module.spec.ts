import { ChannelRoutingModule } from './channel-routing.module';

describe('ChannelRoutingModule', () => {
  let channelRoutingModule: ChannelRoutingModule;

  beforeEach(() => {
    channelRoutingModule = new ChannelRoutingModule();
  });

  it('should create an instance', () => {
    expect(channelRoutingModule).toBeTruthy();
  });
});
