﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KaplanCodeAssignment.DTO.Base
{
    public class BaseResponse
    {
        public bool IsSuccess
        {
            get
            {
                return !ErrorMessages.Any();
            }
        }

        public List<string> ErrorMessages = new List<string>();
    }
}