﻿using KaplanCodeAssignment.DTO.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KaplanCodeAssignment.DTO.DTOResponse
{
    public class ChannelDTOResponse : BaseResponse
    {
        public List<ChannelInfo> ChannelList { get; set; }
    }
    public class ChannelInfo
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string InstructorName { get; set; }
        public string InstructorPhotoUrl { get; set; }
        public string Time { get; set; }
    }
}