﻿using KaplanCodeAssignment.DTO.DTOResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KaplanCodeAssignment.Services.Interface
{
    public interface IChannelServices
    {
        /// <summary>
        /// Get Channel session information
        /// </summary>
        /// <returns>ChannelDTOResponse</returns>
        ChannelDTOResponse GetChannelList();

        // <summary>
        /// Get channel data source file path
        /// </summary>
        /// <returns></returns>
        string GetChannelSourceDataFilePath();
    }
}