﻿using KaplanCodeAssignment.DTO.DTOResponse;
using KaplanCodeAssignment.Services.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace KaplanCodeAssignment.Services
{
    public class ChannelServices : IChannelServices
    {
        private static ChannelServices _service = null;
        private static object lockObject = new object();

        public static ChannelServices Service
        {
            get
            {
                if (_service == null)
                {
                    lock (lockObject)
                    {
                        _service = new ChannelServices();
                    }
                }
                return _service;
            }
        }

        /// <summary>
        /// Get Channel session information
        /// </summary>
        /// <returns>ChannelDTOResponse</returns>
        public ChannelDTOResponse GetChannelList()
        {
            ChannelDTOResponse result = new ChannelDTOResponse();
            try
            {
                string filePath = GetChannelSourceDataFilePath();
                using (StreamReader r = new StreamReader(filePath))
                {
                    string jsonData = r.ReadToEnd();
                    result.ChannelList = JsonConvert.DeserializeObject<List<ChannelInfo>>(jsonData);
                }
            }
            catch (Exception ex)
            {
                // write a failed log here if required
                result.ErrorMessages.Add("Failed to get channel list");
            }
            return result;
        }

        /// <summary>
        /// Get channel data source file path
        /// </summary>
        /// <returns></returns>
        public string GetChannelSourceDataFilePath()
        {
            try
            {
                string currentDirectoryPath = HttpContext.Current.Server.MapPath("~");
                return currentDirectoryPath + ConfigurationManager.AppSettings["dataSource"].ToString();
            }
            catch
            {
                /// This is added to achive the unit test
                return null;
            }
        }
    }
}