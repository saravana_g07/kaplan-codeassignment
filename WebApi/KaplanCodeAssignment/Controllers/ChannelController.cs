﻿using KaplanCodeAssignment.Controllers.Base;
using KaplanCodeAssignment.DTO.DTOResponse;
using KaplanCodeAssignment.Services;
using KaplanCodeAssignment.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace KaplanCodeAssignment.Controllers
{
    [RoutePrefix("api/Channel")]
    public class ChannelController : BaseApiController
    {
        private readonly IChannelServices channelServices;

        public ChannelController()
        {
            this.channelServices = ChannelServices.Service;
        }

        /// <summary>
        /// Get channel session information list
        /// </summary>
        /// <returns></returns>
        [Route("getChannelList")]
        public IHttpActionResult GetChannelList()
        {
            ChannelDTOResponse result = new ChannelDTOResponse();
            try
            {
                result = channelServices.GetChannelList();
            }
            catch (Exception ex)
            {
                // can be add/restrict the information based on the requirement
                result.ErrorMessages.Add("Exception occuerd while getting channel list");
                result.ErrorMessages.Add("Exception Message" + ex.Message);
            }

            return Ok(result);

        }
    }
}