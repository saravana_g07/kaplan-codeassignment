﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KaplanCodeAssignment.Services;
using KaplanCodeAssignment.DTO.DTOResponse;
using System.Collections.Generic;
using System.Linq;

namespace KaplanCodeAssignmentTest
{
    [TestClass]
    public class ChannelServiceTest
    {
        [TestMethod]
        public void GetChannelListTest()
        {
            string filePath = ChannelServices.Service.GetChannelSourceDataFilePath();
            ChannelDTOResponse channelList = ChannelServices.Service.GetChannelList();
            ChannelDTOResponse result = ChannelServices.Service.GetChannelList();
            if (filePath == null)
            {
                Assert.IsTrue(result.ErrorMessages.First() == "Failed to get channel list", "ReadJsonChannelResultTest passes successfully");
            }
            else
            {
                Assert.IsTrue(result.ChannelList.Count > 0, "ReadJsonChannelResultTest passes successfully");
            }
        }
    }
}
